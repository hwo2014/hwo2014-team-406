var net = require("net");
var JSONStream = require('JSONStream');
var guiserver = require('./guiserver');
var Strategist = require('./strategist');
var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

if(botName.indexOf('gui') !== -1){
  guiserver.initialize();
}

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

var carcolor;
var strategist = new Strategist();

jsonStream = client.pipe(JSONStream.parse());
var stateIndex = 0;
jsonStream.on('data', function(data) {

  if (data.msgType === 'carPositions') {
    guiserver.send('state', {
        index: stateIndex++,
        carPositions: data.data,
        meta: strategist.meta
    });

    data.data.forEach(function(carpos) {
      if(carpos.id.color === carcolor) {
        strategist.handlePosition(carpos);
      }
    });
    var sw = strategist.getLaneSwitch();
    if(sw) {
      console.log("LANE SWITCH", sw);
      send({
        msgType: "switchLane",
        data: sw
      });
      
    } else {
      send({
        msgType: "throttle",
        data: strategist.throttle
      });
    }
  } else {
    if (data.msgType === 'join') {
      console.log('Joined');
    } else if (data.msgType === 'gameInit') {
      strategist.setTrack(data.data.race.track);
      guiserver.send('gameInit', data.data);
    } else if (data.msgType === 'yourCar') {
      carcolor = data.data.color;
      console.log('my car is '+carcolor);
      guiserver.send('yourCar', data.data);
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
      stateIndex = 0 ;
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } else {
      console.log('MSG', data.msgType, data.data);
    }

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
