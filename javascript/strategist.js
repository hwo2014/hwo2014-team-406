var Strategist = module.exports = function () {
	this.in_turn = false;
	this.next_turn = null;
	this.lanes = [];
	this.throttle = 0.65;
	this.lane_switch = null;
	this.current_lane = null;
	this.current_piece_index = null;
	this.current_car_position_index = -1;
	this.car_positions = [];
	this.prev_pos;
	this.prev_angle;
};

Strategist.prototype.getLaneSwitch = function() {
	var sw = this.lane_switch;
	this.lane_switch = null;
	return sw;
}

Strategist.prototype.setTrack = function(track) {
	this.track = track;
	this.track.lanes.forEach(function(lane){
		this.lanes[lane.index] = lane.distanceFromCenter;
	}, this);
};

Strategist.prototype.maybeTurn = function () {
	var tot_angle = 0;
	var i = this.current_piece_index + 1;
	var piece = this.getPiece(i);
	while( i == this.current_piece_index + 1 || !piece.switch) {
		console.log(i, piece.angle);
		if(piece.angle) tot_angle += piece.angle;
		piece = this.getPiece(++i);
	}

	if(tot_angle) this.lane_switch = tot_angle > 0 ? 'Right' : 'Left';
}

Strategist.prototype.getPiece = function(idx) {
	if(idx === undefined) {
		idx = this.current_piece_index;
	}
	return this.track.pieces[idx % this.track.pieces.length];
}

Strategist.prototype.getPosition = function(idx) {
	if(idx === undefined) {
		idx = this.current_car_position_index;
	}
	return this.car_positions[idx];
}

Strategist.prototype.getPrevPosition = function(idx) {
	return this.getPosition(this.current_car_position_index - 1);
}

Strategist.prototype.getNextPiece = function () {
	return this.getPiece(this.current_piece_index + 1);
}

Strategist.prototype.getNextNextPiece = function () {
	return this.getPiece(this.current_piece_index + 2);
}

Strategist.prototype.getPieceRadius = function(piece, lane_index) {
	if(piece.radius){
		return piece.radius + this.lanes[lane_index] * (piece.angle > 0 ? -1 : 1);
	} else {
		return 0;
	}
}

Strategist.prototype.getPieceLength = function(piece, lane_index) {
	if(piece.length) {
		return piece.length;
	} else {
		return 2 * Math.PI * this.getPieceRadius(piece, lane_index) * (Math.abs(piece.angle) / 360)
	}
};

Strategist.prototype.getSpeed = function (idx) {
	if(idx === undefined) {
		idx = this.current_car_position_index;
	}
	var cur = this.getPosition(idx),
		prev = this.getPosition(idx-1);

	var speed = 0;
	if(cur && prev) {
		if(cur.piecePosition.pieceIndex === prev.piecePosition.pieceIndex) {
			speed =  cur.piecePosition.inPieceDistance - prev.piecePosition.inPieceDistance;
		} else {
			var prev_piece = this.getPiece(prev.piecePosition.pieceIndex);
			speed = this.getPieceLength(prev_piece, prev.piecePosition.lane.endLaneIndex)  - prev.piecePosition.inPieceDistance + cur.piecePosition.inPieceDistance;
		}
	}

	if(idx = this.current_car_position_index && this.prev_speed && Math.abs(speed - this.prev_speed) > 0.3){
		speed = this.prev_speed;
	}
	return speed;
}	

Strategist.prototype.getDeltaAngle = function () {
	var cur = this.getPosition(),
		prev = this.getPrevPosition();

	if(cur && prev) { 
		return Math.abs(Math.abs(cur.angle) - Math.abs(prev.angle));
	}
	return 0;
}

Strategist.prototype.getDeltaSpeed = function () {
	return this.getSpeed() - this.getSpeed(this.current_car_position_index -1);
}

Strategist.prototype.fillMeta = function () {
	this.meta = {
		throttle: this.throttle,
		angle: this.getPosition().angle,
		delta_angle: this.getDeltaAngle(),
		speed: this.getSpeed(),
		delta_speed: this.getDeltaSpeed(),
		radius: this.getPieceRadius(this.getPiece(), this.getPosition().piecePosition.lane.endLaneIndex),
		ofst: this.lanes[this.getPosition().piecePosition.lane.endLaneIndex],
		piece_angle: this.getPiece().angle || 0,
		to_next_turn: this.getDistanceToNextTurn(),
		brake_dist: this.getBrakeDist()
	};
}

Strategist.prototype.getDistanceToNextTurn = function () {
	var dist = 0;
	var i = this.current_piece_index;
	while(this.getPiece(i).length) {
		if(i == this.current_piece_index) {
			dist += this.getPiece(i).length - this.getPosition().piecePosition.inPieceDistance
		} else {
			dist += this.getPiece(i).length;
		}
		i++;
	}
	return dist;
}

Strategist.prototype.getOptimalTurnSpeed =  function(radius) {
	//TODO
	if(radius < 150) return 6.55;
	return 10;
};

Strategist.prototype.getBrakeDist = function () {
	//TODO
	
	var v= Math.max(0, (this.getSpeed() - this.getOptimalTurnSpeed(this.getPieceRadius(this.next_turn, this.current_lane)))*40);
	return v;
};

Strategist.prototype.brakeIfTurnIsNearby = function () {
	var dist = this.getDistanceToNextTurn();
	var brake_dist = this.getBrakeDist();
	if(dist > 0 && dist < brake_dist && this.getSpeed() > this.getOptimalTurnSpeed(this.getPieceRadius(this.next_turn, this.current_lane))) {
		return 0.0001;
	}
	return false;
}

Strategist.prototype.negotiateTurn = function () {
	var curp = this.getPiece();
	if(curp.angle && curp.radius < 150) {
		var a = this.getPosition().angle;
		var da = this.getDeltaAngle();
		a = Math.abs(a);

		//if(this.getSpeed() - this.getOptimalTurnSpeed(this.getPieceRadius(curp, this.current_lane)) > 0.5) {
		//	return 0.1;
		//}
		//console.log(a, da);
		var optsp = this.getOptimalTurnSpeed(this.getPieceRadius(curp, this.current_lane)) ;
		if(da <= 0.5 && a < 58) {
			return 1;
		} 
		if(da > 1 && a > 50) {
			return 0.001;
		}
		if(da > 2 && a > 35) {
			return 0.001;
		}
		if(da > 3) {
			if(a < 50 && this.getSpeed() < optsp){
				return optsp / 10;
			}
			return 0.001;
		}

		if((da > 2 && a > 20) || (da >1 && a > 40)) {
			//return 1;
			return optsp/ 10;
		}
		if(da > 0 && a > 55){
			if(this.getSpeed() < optsp) {
				return optsp / 10;
			}
			return 0.001;
		}
		return 1;
		return this.getOptimalTurnSpeed(this.getPieceRadius(curp, this.current_lane)) / 10;
		
		


	}
	return false;

}


Strategist.prototype.handlePosition = function(carPosition) {
	this.current_car_position_index++;
	this.car_positions.push(carPosition);
	this.current_lane = carPosition.piecePosition.lane.endLaneIndex;

	var switched_piece = false;

	//detect if we switched pieces
	if(this.current_piece_index == null) {
		this.current_piece_index = carPosition.piecePosition.pieceIndex;
	} else if (this.current_piece_index != carPosition.piecePosition.pieceIndex) {
		this.current_piece_index = carPosition.piecePosition.pieceIndex;
		switched_piece = true;
	}


	if(!this.next_turn || switched_piece) {
		var turn = null;
		var i = this.current_piece_index + 1;
		var piece = this.getPiece(i);
		while(!turn) {
			if(piece.angle) {
				turn = piece;
				break;
			}
			piece = this.getPiece(++i);
		}
		this.next_turn = turn;
		console.log('next turn angle ' +turn.angle);
	}

	if(switched_piece && this.getPiece(this.current_piece_index+1).switch) {
		this.maybeTurn();
	}

	this.throttle = this.brakeIfTurnIsNearby() || this.negotiateTurn() || 1;
	this.prev_speed = this.getSpeed();
	this.fillMeta();

	//console.log('s', this.current_lane, this.current_piece_index, this.throttle, this.lane_switch_in_progress);
};

