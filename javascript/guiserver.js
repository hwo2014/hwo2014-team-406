var WebSocket = require('faye-websocket'),
    http      = require('http');

var server;
    sockets = {};
    nextid = 1,
    history = [];

function initServer() {

  server = http.createServer();

  server.on('upgrade', function(request, socket, body) {
   
    if (WebSocket.isWebSocket(request)) {
      var id = nextid++;
      console.log('gui connected! id '+id);
      var ws = new WebSocket(request, socket, body);
      sockets[id] = ws;

      history.forEach(function(msg){
        ws.send(JSON.stringify(msg));
      });

      ws.on('close', function(event) {
        console.log('close gui socket id '+id, event.code, event.reason);
        delete sockets[id];
        ws = null;
      });
    }
  });

  server.listen(9999);
  console.log('gui server initialized on 9999')
}

module.exports = {
  send : function(message_type, data) {
    var msg = {
        message_type: message_type,
        data: data
    };
    Object.keys(sockets).forEach(function(id) {
      sockets[id].send(JSON.stringify(msg));
    });
    history.push(msg);
  },
  initialize: initServer
};